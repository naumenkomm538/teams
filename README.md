# Teams REST API

---

## Description

This is a REST API for managing teams and their members. It is built using Django and Django Rest Framework.

## Stack

- Python 3.11
- Django 5.0.6
- Django Rest Framework 3.15.2
- Django Filter 24.2
- DRF Spectacular 0.27.2 (Swagger)

## Commands

#### Build image
```bash
docker build -t teams-api:latest .
```

#### Docker stop and remove container
```bash
docker stop teams-api && docker rm teams-api
```

#### Run container
```bash
docker run -d \
  -p 8888:8000 \
  --name teams-api \
  teams-api:latest \
  sh -c 'python manage.py migrate && gunicorn --bind 0.0.0.0:8000 --workers 1 --threads 2 core.wsgi:application'

```

#### Run tests
```bash
docker exec -it teams-api python manage.py test -v 3
```

### [Open in browser](http://localhost:8888)


---
![img.png](images/img.png)
