from rest_framework.test import APITestCase

from teams.models import Team
from users.models import CustomUser


class TestAssignPeople(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter(is_superuser=False).first()
        self.client.force_authenticate(self.user)

    def test_assign_people(self):
        team = Team.objects.get(pk=1)
        people_ids = [1, 2, 3]
        response = self.client.post(f"/api/teams/{team.name}/assign-people/", {"people_ids": people_ids})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(CustomUser.objects.filter(team=team).count(), len(people_ids))
        self.assertDictEqual(response.json(), {"message": "People assigned successfully."})

    def test_assign_people_invalid_people_ids(self):
        team = Team.objects.get(pk=1)
        count = CustomUser.objects.filter(team=team).count()
        people_ids = [1, 2, 3, 4]
        response = self.client.post(f"/api/teams/{team.name}/assign-people/", {"people_ids": people_ids})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(CustomUser.objects.filter(team=team).count(), count)
        self.assertDictEqual(response.json(), {"people_ids": ["Users with ids 4 do not exist."]})
