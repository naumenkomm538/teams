from drf_spectacular.utils import extend_schema
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from users.models import CustomUser
from users.serializers import UsersIdsSerializer
from .models import Team
from .serializers import TeamSerializer


@extend_schema(tags=["Team"])
class TeamViewSet(viewsets.ModelViewSet):
    lookup_field = "name"
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

    def get_serializer_class(self):
        if self.action == "assign_people":
            return UsersIdsSerializer
        return super().get_serializer_class()

    @action(detail=True, methods=["post"], url_path="assign-people")
    def assign_people(self, request, *args, **kwargs):
        team = self.get_object()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        people_ids = serializer.validated_data["people_ids"]
        CustomUser.objects.filter(id__in=people_ids).update(team=team)

        return Response(status=status.HTTP_200_OK, data={"message": "People assigned successfully."})
