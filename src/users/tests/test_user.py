from rest_framework.test import APITestCase

from users.models import CustomUser


class TestRetrieve(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter(is_superuser=False).first()
        self.client.force_authenticate(self.user)

    def test_retrieve_user(self):
        expected = CustomUser.objects.get(pk=1)
        response = self.client.get("/api/users/1/")
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.json(),
            {
                "id": expected.id,
                "username": expected.username,
                "first_name": expected.first_name,
                "last_name": expected.last_name,
                "email": expected.email,
                "user_type": expected.user_type,
                "team": expected.team.name,
            },
        )


class TestList(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter(is_superuser=False).first()
        self.client.force_authenticate(self.user)

    def test_list_users(self):
        expected = CustomUser.objects.order_by("username")[:2]
        response = self.client.get("/api/users/", {"ordering": "username", "size": 2})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()["results"]), len(expected))
        self.assertDictEqual(
            response.json()["results"][0],
            {
                "id": expected[0].id,
                "username": expected[0].username,
                "first_name": expected[0].first_name,
                "last_name": expected[0].last_name,
                "email": expected[0].email,
                "user_type": expected[0].user_type,
                "team": expected[0].team.name,
            },
        )
        self.assertDictEqual(
            response.json()["results"][1],
            {
                "id": expected[1].id,
                "username": expected[1].username,
                "first_name": expected[1].first_name,
                "last_name": expected[1].last_name,
                "email": expected[1].email,
                "user_type": expected[1].user_type,
                "team": expected[1].team.name,
            },
        )


class TestCreate(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter().first()
        self.client.force_authenticate(self.user)

    def test_create_user(self):
        data = {
            "username": "test",
            "first_name": "test",
            "last_name": "test",
            "email": "m@m.com",
            "user_type": "doctor",
            "team": "Foxes",
        }
        response = self.client.post("/api/users/", data=data)
        self.assertEqual(response.status_code, 201, response.content)
        self.assertDictEqual(
            response.json(),
            {
                "username": "test",
                "first_name": "test",
                "last_name": "test",
                "id": 4,
                "email": "m@m.com",
                "user_type": "doctor",
                "team": "Foxes",
            },
        )

    def test_fail_create_user(self):
        data = {
            "username": "test",
            "first_name": "test",
            "last_name": "test",
        }
        response = self.client.post("/api/users/", data=data)
        self.assertEqual(response.status_code, 400)


class TestUpdate(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter().first()
        self.client.force_authenticate(self.user)

    def test_update_user(self):
        data = {
            "username": "changed",
            "first_name": "changed",
            "last_name": "changed",
            "email": "m@m.com",
            "user_type": "patient",
            "team": "Foxes",
        }
        response = self.client.put("/api/users/1/", data=data)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.json(),
            {
                "username": "changed",
                "first_name": "changed",
                "last_name": "changed",
                "id": 1,
                "email": "m@m.com",
                "user_type": "patient",
                "team": "Foxes",
            },
        )

    def test_fail_update_user(self):
        data = {
            "username": "changed",
            "first_name": "changed",
            "last_name": "changed",
        }
        response = self.client.put("/api/users/1/", data=data)
        self.assertEqual(response.status_code, 400)


class TestPartialUpdate(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter().first()
        self.client.force_authenticate(self.user)

    def test_partial_update_user(self):
        data = {
            "last_name": "changed",
            "user_type": "patient",
        }
        response = self.client.patch("/api/users/1/", data=data)
        self.assertEqual(response.status_code, 200)
        json_ = response.json()
        self.assertTrue(json_["last_name"], "changed")
        self.assertTrue(json_["user_type"], "patient")


class TestDelete(APITestCase):
    fixtures = ["teams", "users"]

    def setUp(self):
        self.user = CustomUser.objects.filter().first()
        self.client.force_authenticate(self.user)

    def test_delete_user(self):
        user = CustomUser.objects.get(pk=1)
        response = self.client.delete("/api/users/1/")
        self.assertEqual(response.status_code, 204)
        self.assertFalse(CustomUser.objects.filter(pk=user.pk).exists())

    def test_fail_delete_user(self):
        response = self.client.delete("/api/users/11/")
        self.assertEqual(response.status_code, 404)
