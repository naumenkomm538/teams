from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.utils.translation import gettext_lazy as _

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser


class CustomUserAdmin(BaseUserAdmin):
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    change_password_form = AdminPasswordChangeForm
    readonly_fields = ("last_login", "date_joined")

    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
        (_("Additional info"), {"fields": ("user_type", "team")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
        (_("Permissions"), {"fields": ("is_active", "is_staff", "is_superuser", "groups", "user_permissions")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2", "user_type", "team"),
            },
        ),
    )
    list_display = ("username", "email", "first_name", "last_name", "user_type", "team", "is_staff")
    search_fields = ("username", "first_name", "last_name", "email")
    ordering = ("username",)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields["password"].help_text = (
                "Raw passwords are not stored, so there is no way to see this user's password, "
                'but you can change the password using <a href="{}">this form</a>.'
            ).format("../password/")
        return form


admin.site.register(CustomUser, CustomUserAdmin)
