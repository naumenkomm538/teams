from rest_framework import serializers

from teams.models import Team
from .models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):
    team = serializers.SlugRelatedField(slug_field="name", queryset=Team.objects.all())

    class Meta:
        model = CustomUser
        fields = ["id", "username", "first_name", "last_name", "email", "user_type", "team"]
        read_only_fields = ["id"]


class UsersIdsSerializer(serializers.Serializer):
    people_ids = serializers.ListField(child=serializers.IntegerField())

    def validate_people_ids(self, value):  # noqa
        """
        Check all people ids exist.
        Return people ids who do not exist.

        """
        people_ids = CustomUser.objects.filter(id__in=value).values_list("id", flat=True)
        not_exists = set(value) - set(people_ids)

        if not_exists:
            not_exists = ", ".join(map(str, not_exists))
            raise serializers.ValidationError(f"Users with ids {not_exists} do not exist.")

        return value
