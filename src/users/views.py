from drf_spectacular.utils import extend_schema
from rest_framework import viewsets, filters

from .models import CustomUser
from .serializers import CustomUserSerializer


@extend_schema(tags=["User"])
class CustomUserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    filter_backends = [filters.OrderingFilter]
    serializer_class = CustomUserSerializer
    ordering_fields = ["username", "user_type", "team__name"]
    ordering = ["username"]
