from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    USER_TYPE_CHOICES = (
        ("patient", "Patient"),
        ("doctor", "Doctor"),
    )
    user_type = models.CharField(max_length=10, choices=USER_TYPE_CHOICES, default="patient")
    team = models.ForeignKey("teams.Team", related_name="members", on_delete=models.SET_NULL, null=True, blank=True)
