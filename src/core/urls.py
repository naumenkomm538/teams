from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView, SpectacularRedocView

urlpatterns = [
    path("admin/", admin.site.urls),
    # Docs
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path("api/schema/docs/", SpectacularSwaggerView.as_view(), name="docs"),
    path("api/schema/redoc/", SpectacularRedocView.as_view(), name="redoc"),
    path("", RedirectView.as_view(url="api/schema/docs/"), name="to_docs"),
    # Apps
    path("api/", include("users.urls")),
    path("api/", include("teams.urls")),
]
