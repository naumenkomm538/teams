FROM python:3.11.4-slim as python-layer
ENV PYTHONUNBUFFERED=true

FROM python-layer as poetry-layer
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:$PATH"
RUN python -c 'from urllib.request import urlopen; print(urlopen("https://install.python-poetry.org").read().decode())' | python -
COPY ./src /opt/src
COPY poetry.lock pyproject.toml ./
RUN poetry install --no-interaction --no-ansi -vvv

FROM python-layer as runtime
ENV PATH="/.venv/bin:$PATH"
COPY --from=poetry-layer /opt/src /opt/src
COPY --from=poetry-layer /.venv /.venv

WORKDIR /opt/src

CMD exec gunicorn --bind 0.0.0.0:8000 --workers 1 --threads 2 core.wsgi:application
